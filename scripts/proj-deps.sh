#!/bin/bash

set -e

eval $(opam env)

opam install --yes --deps-only src/
