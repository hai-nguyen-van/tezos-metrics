### Tezos metrics

```
--- tezos_explorer
    --- chain
        --- head_level
    --- storage
        --- total
        --- store
        --- context
        --- context_index
    --- p2p
        --- connections
        --- peers
        --- points
```

`./tezos-metrics --listen-prometheus=9091 --data-dir ~/.tezos-node/ -h localhost -p 18732`

## Using docker

We can use `teztool` to test `tezos-metrics` in a sandbox.

    $teztool create sandbox  \
      --baker-identity bootstrap1 \
      --sandbox-time-between-blocks 10 \
      --network sandbox \
      --rpc-port 18732 \
      --historyMode rolling

    $docker run -it registry.gitlab.com/nomadic-labs/tezos-metrics --help
