(** the type of a metric extractor *)
type t =
  | Extractor : {
      name : string;
      init : namespace:string -> string -> 's;
      update : 's -> unit Lwt.t;
    }
      -> t

(** The signature of a set of metrics for a subsystem *)
module type S = sig
  val subsystem : string

  val all : t list
end

(** The signature for a generic metric *)
module type T = sig
  module Config : Config_sig.S

  val name : string

  val path : string

  val help : string

  val delay : float

  val member : string option

  val subsystem : string
end

(** The signature of the extractor functions for a generic metric *)
module type U = sig
  type t

  val init : namespace:string -> string -> t

  val set : t -> string -> unit
end

(** the signature of the extractor functions for a metric using a command *)
module type V = sig
  include U

  val cmd : string * string array
end
