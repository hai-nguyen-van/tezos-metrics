module type S = sig
  val uri : Uri.t

  val delay : float

  val data_dir : string

  val p2p : bool

  val chain : bool

  val memory : bool

  val workers : bool

  val mempool : bool
end
