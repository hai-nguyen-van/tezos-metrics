module MempoolPendingMetric = struct
  type t = {
    applied : Prometheus.Gauge.t;
    refused : Prometheus.Gauge.t;
    branch_refused : Prometheus.Gauge.t;
    branch_delayed : Prometheus.Gauge.t;
    unprocessed : Prometheus.Gauge.t;
  }

  let decode s =
    let json = Yojson.Safe.from_string s in
    let count f =
      Yojson.Safe.Util.(json |> member f |> to_list |> List.length)
    in
    ( count "applied",
      count "refused",
      count "branch_refused",
      count "branch_delayed",
      count "unprocessed" )

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v ~namespace ~help:"Mempool" ~subsystem:"mempool" name
    in
    {
      applied = g (name ^ "_applied");
      refused = g (name ^ "_refused");
      branch_refused = g (name ^ "_branch_refused");
      branch_delayed = g (name ^ "_branch_delayed");
      unprocessed = g (name ^ "_unprocessed");
    }

  let set store s =
    let (applied, refused, branch_refused, branch_delayed, unprocessed) =
      decode s
    in
    Prometheus.Gauge.set store.applied (float applied) ;
    Prometheus.Gauge.set store.refused (float refused) ;
    Prometheus.Gauge.set store.branch_refused (float branch_refused) ;
    Prometheus.Gauge.set store.branch_delayed (float branch_delayed) ;
    Prometheus.Gauge.set store.unprocessed (float unprocessed)
end

module MakePendingMempool (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (MempoolPendingMetric)

module Make (Config : Config_sig.S) : Collections_sig.S = struct
  let delay = 20.

  let subsystem = "mempool"

  module Mempool = MakePendingMempool (struct
    module Config = Config

    let name = "pending"

    let path = "/chains/main/mempool/pending_operations"

    let help = "mempool pending operations"

    let member = None

    let subsystem = subsystem

    let delay = delay
  end)

  let all = [Mempool.l]
end
