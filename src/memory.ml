module MemoryMetric = struct
  type t = {
    size : Prometheus.Gauge.t;
    resident : Prometheus.Gauge.t;
    shared : Prometheus.Gauge.t;
    text : Prometheus.Gauge.t;
    lib : Prometheus.Gauge.t;
    data : Prometheus.Gauge.t;
  }

  let decode s =
    let json = Yojson.Safe.from_string s in
    let size = JsonUtils.to_int64 "size" json in
    let resident = JsonUtils.to_int64 "resident" json in
    let shared = JsonUtils.to_int64 "shared" json in
    let text = JsonUtils.to_int64 "text" json in
    let lib = JsonUtils.to_int64 "lib" json in
    let data = JsonUtils.to_int64 "data" json in
    (size, resident, shared, text, lib, data)

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v
        ~namespace
        ~help:"Node Memory stats"
        ~subsystem:"stats"
        name
    in
    {
      size = g (name ^ "_size");
      resident = g (name ^ "_resident");
      shared = g (name ^ "_shared");
      text = g (name ^ "_text");
      lib = g (name ^ "_lib");
      data = g (name ^ "_data");
    }

  let set store s =
    let (size, resident, shared, text, lib, data) = decode s in
    Prometheus.Gauge.set store.size (Int64.to_float size) ;
    Prometheus.Gauge.set store.resident (Int64.to_float resident) ;
    Prometheus.Gauge.set store.shared (Int64.to_float shared) ;
    Prometheus.Gauge.set store.text (Int64.to_float text) ;
    Prometheus.Gauge.set store.lib (Int64.to_float lib) ;
    Prometheus.Gauge.set store.data (Int64.to_float data)
end

module MakeMemoryMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (MemoryMetric)

module Make (Config : Config_sig.S) : Collections_sig.S = struct
  let subsystem = "stats"

  let delay = 30.

  module Memory = MakeMemoryMetric (struct
    module Config = Config

    let name = "memory"

    let path = "/stats/memory"

    let member = None

    let help = "Node memory consumption"

    let subsystem = subsystem

    let delay = delay
  end)

  let all = [Memory.l]
end
