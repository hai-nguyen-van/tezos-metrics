module HeadMetrics = struct
  type t = {level : Prometheus.Gauge.t; cycle : Prometheus.Gauge.t}

  let decode s =
    let json = Yojson.Safe.from_string s in
    let level = Yojson.Safe.Util.(to_int @@ member "level" json) in
    let cycle = Yojson.Safe.Util.(to_int @@ member "cycle" json) in
    (level, cycle)

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v
        ~namespace
        ~help:"Main chain current level and cycle"
        ~subsystem:"chain"
        name
    in
    {level = g (name ^ "_level"); cycle = g (name ^ "_cycle")}

  let set store s =
    let (level, cycle) = decode s in
    Prometheus.Gauge.set store.level (float level) ;
    Prometheus.Gauge.set store.cycle (float cycle)
end

module CheckpointMetrics = struct
  type t = {save_point : Prometheus.Gauge.t; caboose : Prometheus.Gauge.t}

  let decode s =
    let json = Yojson.Safe.from_string s in
    let save_point = Yojson.Safe.Util.(to_int @@ member "save_point" json) in
    let caboose = Yojson.Safe.Util.(to_int @@ member "caboose" json) in
    (save_point, caboose)

  let init ~namespace name =
    let g name help =
      Prometheus.Gauge.v ~namespace ~help ~subsystem:"chain" name
    in
    {
      save_point = g (name ^ "_save_point") "checkpoint level";
      caboose = g (name ^ "_caboose") "caboose";
    }

  let set store s =
    let (save_point, caboose) = decode s in
    Prometheus.Gauge.set store.save_point (float save_point) ;
    Prometheus.Gauge.set store.caboose (float caboose)
end

module HeadKindsMetrics = struct
  type t = (string * Prometheus.Gauge.t) list

  let kind_list =
    [ "endorsement";
      "seed_nonce_revelation";
      "double_endorsement_evidence";
      "double_baking_evidence";
      "activate_account";
      "proposals";
      "ballot";
      "reveal";
      "transaction";
      "origination";
      "delegation" ]

  let init_kinds () = List.map (fun k -> (k, ref 0)) kind_list

  let decode s =
    let json = Yojson.Safe.from_string s in
    let operations =
      Yojson.Safe.Util.(json |> member "operations" |> to_list)
    in
    let kinds = init_kinds () in
    List.iter
      (fun jl ->
        List.iter
          (fun j ->
            List.iter
              (fun jj ->
                let k = Yojson.Safe.Util.(jj |> member "kind" |> to_string) in
                match List.assoc_opt k kinds with
                | Some c ->
                    incr c
                | None ->
                    failwith ("kind not found: " ^ k))
              Yojson.Safe.Util.(j |> member "contents" |> to_list))
          Yojson.Safe.Util.(jl |> to_list))
      operations ;
    kinds

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v
        ~namespace
        ~help:"Main chain head operations metrics"
        ~subsystem:"chain"
        name
    in
    List.map (fun k -> (k, g (name ^ "_" ^ k))) kind_list

  let set store s =
    List.iter
      (fun (k, v) -> Prometheus.Gauge.set (List.assoc k store) (float !v))
      (decode s)
end

module MakeHeadMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (HeadMetrics)
module MakeHeadKindMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (HeadKindsMetrics)
module MakeCheckpointMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (CheckpointMetrics)

module Make (Config : Config_sig.S) : Collections_sig.S = struct
  let subsystem = "chain"

  let delay = 45.

  module Head = MakeHeadMetric (struct
    module Config = Config

    let name = "current"

    let path = "/chains/main/blocks/head/helpers/current_level"

    let member = None

    let help = "Level and cycles associated with the current head"

    let subsystem = subsystem

    let delay = delay
  end)

  module HeadKind = MakeHeadKindMetric (struct
    module Config = Config

    let name = "head"

    let path = "/chains/main/blocks/head"

    let member = None

    let help = "Metrics associated with the current head"

    let subsystem = subsystem

    let delay = delay
  end)

  module InvalidBlocks = Collections.MakeCountList (struct
    module Config = Config

    let name = "invalid_blocks"

    let path = "/chains/main/invalid_blocks"

    let member = None

    let help = "Number of invalid blocks"

    let subsystem = subsystem

    let delay = delay
  end)

  module Heads = Collections.MakeCountList (struct
    module Config = Config

    let name = "heads"

    let path = "/chains/main/blocks"

    let member = None

    let help = "Number of known heads"

    let subsystem = subsystem

    let delay = delay
  end)

  module Checkpoint = MakeCheckpointMetric (struct
    module Config = Config

    let name = "checkpoint"

    let path = "/chains/main/checkpoint"

    let member = None

    let help = "Checkpoint metrics"

    let subsystem = subsystem

    let delay = 3600.
  end)

  let all = [Head.l; InvalidBlocks.l; Heads.l; HeadKind.l; Checkpoint.l]
end
