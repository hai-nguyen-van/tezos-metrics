module VersionMetrics = struct
  type t = {version : Prometheus.Gauge.family}

  let decode s =
    let json = Yojson.Safe.from_string s in
    let version =
      let v = Yojson.Safe.Util.(member "version" json) in
      let major = Yojson.Safe.Util.(v |> member "major" |> to_int) in
      let minor = Yojson.Safe.Util.(v |> member "minor" |> to_int) in
      let info =
        Yojson.Safe.Util.(v |> member "additional_info" |> to_string)
      in
      Format.sprintf "%i.%i.%s" major minor info
    in
    let network_version =
      Yojson.Safe.Util.(json |> member "network_version")
    in
    let chain_name =
      Yojson.Safe.Util.(network_version |> member "chain_name" |> to_string)
    in
    let distributed_db_version =
      Yojson.Safe.Util.(
        member "distributed_db_version" network_version
        |> to_int |> string_of_int)
    in
    let p2p_version =
      Yojson.Safe.Util.(
        member "p2p_version" network_version |> to_int |> string_of_int)
    in
    let commit = Yojson.Safe.Util.(member "commit_info" json) in
    let commit_hash =
      Yojson.Safe.Util.(
        commit |> member "commit_hash" |> to_string
        |> fun s -> String.sub s 0 8)
    in
    let commit_date =
      Yojson.Safe.Util.(
        commit |> member "commit_date" |> to_string
        |> fun s -> String.sub s 0 10)
    in
    [ version;
      chain_name;
      distributed_db_version;
      p2p_version;
      commit_hash;
      commit_date ]

  let init ~namespace name =
    {
      version =
        Prometheus.Gauge.v_labels
          ~namespace
          ~label_names:
            [ "version";
              "chain_name";
              "distributed_db_version";
              "p2p_version";
              "commit_hash";
              "commit_date" ]
          ~help:"Node version"
          ~subsystem:"info"
          name;
    }

  let set store s =
    let labels = decode s in
    let t = Prometheus.Gauge.labels store.version labels in
    Prometheus.Gauge.set t 1.
end

module MakeVersionMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (VersionMetrics)

module Make (Config : Config_sig.S) : Collections_sig.S = struct
  let subsystem = "info"

  let delay = 3600.

  module Version = MakeVersionMetric (struct
    module Config = Config

    let name = "version"

    let path = "/version"

    let member = None

    let help = "info about the node"

    let subsystem = subsystem

    let delay = delay
  end)

  let all = [Version.l]
end
