module type S = sig
  val update : unit -> unit Lwt.t
end

module type T = sig
  val metrics : (module S) list
end

let rpc_get ~delay ~subsystem fn uri =
  Lwt.catch
    (fun () ->
      Cohttp_lwt_unix.Client.get uri
      >>= fun (_, body) ->
      Cohttp_lwt.Body.to_string body >>= fun s -> fn s ; Lwt.return_unit)
    (fun _ ->
      let new_d = min 30. (2. *. delay) in
      Format.eprintf
        "warning: [log:%s] could not connect to tezos node, trying again in \
         %.0f seconds.@."
        subsystem
        new_d ;
      Lwt_unix.sleep new_d)

let simple_metric ~metric_type ~help ~subsystem name fn =
  let open Prometheus in
  let name = Printf.sprintf "%s_%s" subsystem name in
  let info =
    {MetricInfo.name = MetricName.v name; help; metric_type; label_names = []}
  in
  let collect () =
    Printf.eprintf "Call collect %s\n" name ;
    LabelSetMap.singleton [] [Sample_set.sample (fn ())]
  in
  (info, collect)

let update uri json ~delay ~subsystem () =
  rpc_get ~delay ~subsystem (fun s -> json := Yojson.Safe.from_string s) uri

let decode name json () =
  Printf.eprintf "decode %s\n" name ;
  float Yojson.Safe.Util.(to_int @@ member name !json)

module CounterMetric (M : sig
  module Config : Config_sig.S

  val path : string

  val help : string

  val name : string

  val subsystem : string
end) =
struct
  open Prometheus

  let uri = Uri.with_path M.Config.uri M.path

  let json = ref (Yojson.Safe.from_string "{}")

  let m =
    simple_metric
      ~subsystem:M.subsystem
      ~metric_type:Gauge
      ~help:M.help
      M.name
      (fun () -> float (List.length (Yojson.Safe.Util.to_list !json)))

  let update () =
    update uri json ~delay:M.Config.delay ~subsystem:M.subsystem ()

  let () =
    Printf.eprintf "notice: register %s subsystem\n" M.subsystem ;
    let open CollectorRegistry in
    List.iter (fun (info, collector) -> register default info collector) [m]
end
