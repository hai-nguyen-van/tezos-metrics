module MakeStorage (Config : Config_sig.S) : Collections_sig.S = struct
  let delay = 5.

  let subsystem = "storage"

  module Total = Collections.MakeDirSize (struct
    module Config = Config

    let name = "total"

    let help = "Total size of the tezos data directory"

    let member = None

    let path = ""

    let subsystem = subsystem

    let delay = delay
  end)

  module Store = Collections.MakeDirSize (struct
    module Config = Config

    let name = "store"

    let help = "Total size of the store"

    let member = None

    let path = "store"

    let subsystem = subsystem

    let delay = delay
  end)

  module Context = Collections.MakeDirSize (struct
    module Config = Config

    let name = "context"

    let help = "Total size of the context"

    let member = None

    let path = "context"

    let subsystem = subsystem

    let delay = delay
  end)

  module Index = Collections.MakeDirSize (struct
    module Config = Config

    let name = "index"

    let help = "Total size of the contenxt index"

    let member = None

    let path = "context/index"

    let subsystem = subsystem

    let delay = delay
  end)

  let all = [Total.l; Store.l; Context.l; Index.l]
end
