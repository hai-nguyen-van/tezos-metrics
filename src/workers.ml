let set ~set ?(f = float) l1 l2 =
  List.iter2 (fun store v -> set store (f v)) l1 l2

let set_a ~set ?(f = float) l1 l2 =
  Array.iter2 (fun store v -> set store (f v)) l1 l2

module DDBMetric = struct
  type t = Prometheus.Gauge.t list

  let decode s =
    let json = Yojson.Safe.from_string s in
    let active_chains = JsonUtils.parse_int "active_chains" json in
    let active_connections = JsonUtils.parse_int "active_connections" json in
    let active_peers = JsonUtils.parse_int "active_peers" json in
    let operation_db = Yojson.Safe.Util.(json |> member "operation_db") in
    let operation_db_table_length =
      JsonUtils.parse_int "table_length" operation_db
    in
    let operation_db_scheduler_length =
      JsonUtils.parse_int "scheduler_length" operation_db
    in
    let operations_db = Yojson.Safe.Util.(json |> member "operations_db") in
    let operations_db_table_length =
      JsonUtils.parse_int "table_length" operations_db
    in
    let operations_db_scheduler_length =
      JsonUtils.parse_int "scheduler_length" operations_db
    in
    let block_header_db =
      Yojson.Safe.Util.(json |> member "block_header_db")
    in
    let block_header_db_table_length =
      JsonUtils.parse_int "table_length" block_header_db
    in
    let block_header_db_scheduler_length =
      JsonUtils.parse_int "scheduler_length" block_header_db
    in
    [ active_chains;
      operation_db_table_length;
      operation_db_scheduler_length;
      operations_db_table_length;
      operations_db_scheduler_length;
      block_header_db_table_length;
      block_header_db_scheduler_length;
      active_connections;
      active_peers ]

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v
        ~namespace
        ~help:"DDB internals"
        ~subsystem:"stats"
        name
    in
    [ g (name ^ "_active_chains");
      g (name ^ "_operation_db_table_length");
      g (name ^ "_operation_db_scheduler_length");
      g (name ^ "_operations_db_table_length");
      g (name ^ "_operations_db_scheduler_length");
      g (name ^ "_block_header_db_table_length");
      g (name ^ "_block_header_db_scheduler_length");
      g (name ^ "_active_connections");
      g (name ^ "_active_peers") ]

  let set stores s =
    let values = decode s in
    set ~set:Prometheus.Gauge.set ~f:float stores values
end

type backlog = int array

let init_backlog () = Array.make 6 0

let decode_backlog b json =
  let backlog = Yojson.Safe.Util.(json |> member "backlog" |> to_list) in
  let get_level j = Yojson.Safe.Util.(j |> member "level" |> to_string) in
  let get_events_length j =
    Yojson.Safe.Util.(j |> member "events" |> to_list |> List.length)
  in
  List.iter
    (fun j ->
      match get_level j with
      | "debug" ->
          b.(0) <- get_events_length j
      | "info" ->
          b.(1) <- get_events_length j
      | "notice" ->
          b.(2) <- get_events_length j
      | "error" ->
          b.(3) <- get_events_length j
      | "warning" ->
          b.(4) <- get_events_length j
      | "fatal" ->
          b.(5) <- get_events_length j
      | _ ->
          ())
    backlog

type backlog_metrics = Prometheus.Gauge.t array

let init_backlog_metrics name namespace =
  let g name =
    Prometheus.Gauge.v
      ~namespace
      ~help:"Backlog Events"
      ~subsystem:"stats"
      name
  in
  [| g (name ^ "_backlog_event_debug_length");
     g (name ^ "_backlog_event_info_length");
     g (name ^ "_backlog_event_notice_length");
     g (name ^ "_backlog_event_error_length");
     g (name ^ "_backlog_event_warning_length");
     g (name ^ "_backlog_event_fatal_length") |]

type status = int array

let init_status () = Array.make 5 0

let decode_status t json =
  let get_status j =
    Yojson.Safe.Util.(j |> member "status" |> member "phase" |> to_string)
  in
  match get_status json with
  | "running" ->
      t.(0) <- t.(0) + 1
  | "closed" ->
      t.(1) <- t.(1) + 1
  | "closing" ->
      t.(2) <- t.(2) + 1
  | "launching" ->
      t.(3) <- t.(3) + 1
  | "crashed" ->
      t.(4) <- t.(4) + 1
  | _ ->
      ()

type status_metrics = Prometheus.Gauge.t array

let init_status_metrics name namespace =
  let g name help =
    Prometheus.Gauge.v ~namespace ~help ~subsystem:"stats" name
  in
  [| g (name ^ "_running") "Workers in 'running' state";
     g (name ^ "_closed") "Workers in 'closed' state";
     g (name ^ "_closing") "Workers in 'closing' state";
     g (name ^ "_launching") "Workers in 'launching' state";
     g (name ^ "_crashed") "Workers in 'crashed; state" |]

module FullStatusMetric = struct
  type t = {pending_requests : Prometheus.Gauge.t; backlog : backlog_metrics}

  let decode s =
    let json = Yojson.Safe.from_string s in
    let pending_requests =
      Yojson.Safe.Util.(
        json |> member "pending_requests" |> to_list |> List.length)
    in
    let backlog = init_backlog () in
    decode_backlog backlog json ;
    (pending_requests, backlog)

  let init ~namespace name =
    let g name help =
      Prometheus.Gauge.v ~namespace ~help ~subsystem:"stats" name
    in
    {
      pending_requests =
        g (name ^ "_pending_requests") "Worker pending requests";
      backlog = init_backlog_metrics name namespace;
    }

  let set store s =
    let (pending_requests, backlog_list) = decode s in
    Prometheus.Gauge.set store.pending_requests (float pending_requests) ;
    set_a ~set:Prometheus.Gauge.set store.backlog backlog_list
end

module PrevalidatorWorkersMetric = struct
  type t = status_metrics

  let decode s =
    let json = Yojson.Safe.from_string s in
    let workers = Yojson.Safe.Util.(json |> to_list) in
    let status = init_status () in
    List.iter (decode_status status) workers ;
    status

  let init ~namespace name = init_status_metrics name namespace

  let set store s =
    let status = decode s in
    set_a ~set:Prometheus.Gauge.set ~f:(fun v -> float v) store status
end

module PeerValidatorMetric = struct
  type m = {min : float ref; max : float ref; sum : float ref; len : int}

  let init_m len = {min = ref 0.; max = ref 0.; sum = ref 0.; len}

  type pipeline_metrics = {
    min : Prometheus.Gauge.t;
    max : Prometheus.Gauge.t;
    avg : Prometheus.Gauge.t;
  }

  type t = {
    status : status_metrics;
    fetched_header : pipeline_metrics;
    fetched_block : pipeline_metrics;
  }

  let decode s =
    let json = Yojson.Safe.from_string s in
    let workers = Yojson.Safe.Util.(json |> to_list) in
    let status = init_status () in
    let len = List.length workers in
    let fh = init_m len in
    let fb = init_m len in
    List.iter
      (fun j ->
        decode_status status j ;
        let h =
          Yojson.Safe.Util.(
            j |> member "pipelines" |> member "fetched_headers" |> to_int
            |> float)
        in
        fh.max := max !(fh.max) h ;
        fh.min := min !(fh.min) h ;
        fh.sum := !(fh.sum) +. h ;
        let b =
          Yojson.Safe.Util.(
            j |> member "pipelines" |> member "fetched_headers" |> to_int
            |> float)
        in
        fb.max := max !(fb.max) b ;
        fb.min := min !(fb.min) b ;
        fb.sum := !(fb.sum) +. b)
      workers ;
    (status, fh, fb)

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v
        ~namespace
        ~help:"Peer Validators aggreated metrics"
        ~subsystem:"stats"
        name
    in
    {
      status = init_status_metrics name namespace;
      fetched_header =
        {
          min = g (name ^ "_fetched_header_min");
          max = g (name ^ "_fetched_header_max");
          avg = g (name ^ "_fetched_header_avg");
        };
      fetched_block =
        {
          min = g (name ^ "_fetched_block_min");
          max = g (name ^ "_fetched_block_max");
          avg = g (name ^ "_fetched_block_avg");
        };
    }

  let set store s =
    let (status, fh, fb) = decode s in
    set_a ~set:Prometheus.Gauge.set store.status status ;
    Prometheus.Gauge.set store.fetched_header.min !(fh.min) ;
    Prometheus.Gauge.set store.fetched_header.max !(fh.max) ;
    Prometheus.Gauge.set store.fetched_header.avg (!(fh.sum) /. float fh.len) ;
    Prometheus.Gauge.set store.fetched_block.min !(fb.min) ;
    Prometheus.Gauge.set store.fetched_block.max !(fb.max) ;
    Prometheus.Gauge.set store.fetched_block.avg (!(fb.sum) /. float fb.len)
end

module MakeDDBMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (DDBMetric)
module MakeBlockValidatorMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (FullStatusMetric)
module MakePeerValidatorMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (PeerValidatorMetric)
module MakePrevalidatorMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (FullStatusMetric)
module MakePrevalidatorWorkersMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (PrevalidatorWorkersMetric)
module MakeChainValidatorMetric (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (FullStatusMetric)

module Make (Config : Config_sig.S) : Collections_sig.S = struct
  let subsystem = "workers"

  let delay = 10.

  module BlockValidator = MakeBlockValidatorMetric (struct
    module Config = Config

    let name = "block_validator"

    let path = "/workers/block_validator"

    let member = Some "pending_requests"

    let help = "Pending request of the block validator"

    let subsystem = subsystem

    let delay = delay
  end)

  module MainChainValidator = MakeChainValidatorMetric (struct
    module Config = Config

    let name = "main_chain_validators"

    let path = "/workers/chain_validators/main"

    let member = Some "pending_requests"

    let help = "Pending request of the main chain_validator"

    let subsystem = subsystem

    let delay = delay
  end)

  module MainChainValidatorDDB = MakeDDBMetric (struct
    module Config = Config

    let name = "main_chain_validators_ddb"

    let path = "/workers/chain_validators/main/ddb"

    let member = None

    let help = "Status of DDB of the main chain_validator"

    let subsystem = subsystem

    let delay = delay
  end)

  module MainChainValidatorPeerValidators = MakePeerValidatorMetric (struct
    module Config = Config

    let name = "main_chain_validators_peer_validators"

    let path = "/workers/chain_validators/main/peers_validators"

    let member = None

    let help =
      "Status of the peer validators associated to the chain validator"

    let subsystem = subsystem

    let delay = delay
  end)

  module MainChainPrevalidator = MakePrevalidatorMetric (struct
    module Config = Config

    let name = "main_chain_prevalidators"

    let path = "/workers/prevalidators/main"

    let member = None

    let help = "Pending request of the main prevalidator"

    let subsystem = subsystem

    let delay = delay
  end)

  module MainChainPrevalidatorWorkers = MakePrevalidatorWorkersMetric (struct
    module Config = Config

    let name = "main_chain_prevalidators_workers"

    let path = "/workers/prevalidators"

    let member = None

    let help = "Pending request of the main prevalidator workers"

    let subsystem = subsystem

    let delay = delay
  end)

  let all =
    [ BlockValidator.l;
      MainChainValidator.l;
      MainChainValidatorDDB.l;
      MainChainValidatorPeerValidators.l;
      MainChainPrevalidator.l;
      MainChainPrevalidatorWorkers.l ]
end
