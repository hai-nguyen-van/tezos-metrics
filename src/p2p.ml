module NetworkStatusPeersMetric = struct
  type t = {
    accepted : Prometheus.Gauge.t;
    running : Prometheus.Gauge.t;
    disconnected : Prometheus.Gauge.t;
  }

  let decode s =
    let json = Yojson.Safe.from_string s in
    let (accepted, running, disconnected) =
      List.fold_left
        (fun (a, r, d) j ->
          match
            Yojson.Safe.Util.(j |> index 1 |> member "state" |> to_string)
          with
          | "accepted" ->
              (a + 1, r, d)
          | "running" ->
              (a, r + 1, d)
          | "disconnected" ->
              (a, r, d + 1)
          | _ ->
              (a, r, d))
        (0, 0, 0)
        (Yojson.Safe.Util.to_list json)
    in
    (accepted, running, disconnected)

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v ~namespace ~help:"Network p2p" ~subsystem:"p2p" name
    in
    {
      accepted = g (name ^ "_accepted");
      running = g (name ^ "_running");
      disconnected = g (name ^ "_disconnected");
    }

  let set store s =
    let (accepted, running, disconnected) = decode s in
    Prometheus.Gauge.set store.accepted (float accepted) ;
    Prometheus.Gauge.set store.running (float running) ;
    Prometheus.Gauge.set store.disconnected (float disconnected)
end

module NetworkStatusPointsMetric = struct
  type t = {
    trusted : Prometheus.Gauge.t;
    greylisted : Prometheus.Gauge.t;
    accepted : Prometheus.Gauge.t;
    running : Prometheus.Gauge.t;
    disconnected : Prometheus.Gauge.t;
  }

  let decode s =
    let json = Yojson.Safe.from_string s in
    let (trusted, greylisted, accepted, running, disconnected) =
      List.fold_left
        (fun (t, g, a, r, d) j ->
          let el = Yojson.Safe.Util.(j |> index 1) in
          let (a, r, d) =
            match
              Yojson.Safe.Util.(
                el |> member "state" |> member "event_kind" |> to_string)
            with
            | "accepted" ->
                (a + 1, r, d)
            | "running" ->
                (a, r + 1, d)
            | "disconnected" ->
                (a, r, d + 1)
            | _ ->
                (a, r, d)
          in
          let t =
            if Yojson.Safe.Util.(el |> member "trusted" |> to_bool) then t + 1
            else t
          in
          let g =
            match Yojson.Safe.Util.(el |> member "greylisted_until") with
            | `Null ->
                g
            | _ ->
                g + 1
          in
          (t, g, a, r, d))
        (0, 0, 0, 0, 0)
        (Yojson.Safe.Util.to_list json)
    in
    (trusted, greylisted, accepted, running, disconnected)

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v ~namespace ~help:"Network p2p" ~subsystem:"p2p" name
    in
    {
      trusted = g (name ^ "_trusted");
      greylisted = g (name ^ "_greylisted");
      accepted = g (name ^ "_accepted");
      running = g (name ^ "_running");
      disconnected = g (name ^ "_disconnected");
    }

  let set store s =
    let (trusted, greylisted, accepted, running, disconnected) = decode s in
    Prometheus.Gauge.set store.trusted (float trusted) ;
    Prometheus.Gauge.set store.greylisted (float greylisted) ;
    Prometheus.Gauge.set store.accepted (float accepted) ;
    Prometheus.Gauge.set store.running (float running) ;
    Prometheus.Gauge.set store.disconnected (float disconnected)
end

module NetworkStatusConnectionsMetric = struct
  type t = {
    total : Prometheus.Gauge.t;
    incoming : Prometheus.Gauge.t;
    private_connection : Prometheus.Gauge.t;
  }

  let decode s =
    let json = Yojson.Safe.from_string s in
    let l = Yojson.Safe.Util.to_list json in
    let (incoming, private_connection) =
      List.fold_left
        (fun (i, p) j ->
          let i =
            if Yojson.Safe.Util.(j |> member "incoming" |> to_bool) then i + 1
            else i
          in
          let p =
            if Yojson.Safe.Util.(j |> member "private" |> to_bool) then p + 1
            else p
          in
          (i, p))
        (0, 0)
        l
    in
    let total = List.length l in
    (total, incoming, private_connection)

  let init ~namespace name =
    let g name =
      Prometheus.Gauge.v ~namespace ~help:"Network p2p" ~subsystem:"p2p" name
    in
    {
      total = g (name ^ "_total");
      incoming = g (name ^ "_incoming");
      private_connection = g (name ^ "_private");
    }

  let set store s =
    let (total, incoming, private_connection) = decode s in
    Prometheus.Gauge.set store.total (float total) ;
    Prometheus.Gauge.set store.incoming (float incoming) ;
    Prometheus.Gauge.set store.private_connection (float private_connection)
end

module MakeCountPeersNetwork (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (NetworkStatusPeersMetric)
module MakeCountPointsNetwork (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (NetworkStatusPointsMetric)
module MakeCountConnectionsNetwork (M : Collections_sig.T) =
  Collections.MakeRpcMetric (M) (NetworkStatusConnectionsMetric)

module Make (Config : Config_sig.S) : Collections_sig.S = struct
  let delay = 20.

  let subsystem = "p2p"

  module Connections = MakeCountConnectionsNetwork (struct
    module Config = Config

    let name = "connections"

    let path = "/network/connections"

    let help = "Number of open p2p connections"

    let member = None

    let subsystem = subsystem

    let delay = delay
  end)

  module Points = MakeCountPointsNetwork (struct
    module Config = Config

    let name = "points"

    let path = "/network/points"

    let help = "Number of connected points"

    let member = None

    let subsystem = subsystem

    let delay = delay
  end)

  module Peers = MakeCountPeersNetwork (struct
    module Config = Config

    let name = "peers"

    let path = "/network/peers"

    let help = "Number of connected peers"

    let member = None

    let subsystem = subsystem

    let delay = delay
  end)

  let all = [Connections.l; Points.l; Peers.l]
end
