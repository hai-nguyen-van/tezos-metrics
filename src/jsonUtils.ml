(** Same as Yojson.Safe.Util.member but accepts fields as "field.subfield" *)
let member s json =
  List.fold_left
    (fun json m -> Yojson.Safe.Util.member m json)
    json
    (String.split_on_char '.' s)

let parse_int m json = member m json |> Yojson.Safe.Util.to_int

let to_int64 m json =
  member m json |> Yojson.Safe.Util.to_string |> Int64.of_string
