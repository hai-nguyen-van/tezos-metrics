class client ~rpc_config =
  object
    inherit Client_context_unix.unix_logger ~base_dir:"/tmp/"

    inherit
      Tezos_rpc_http_client_unix.RPC_client_unix.http_ctxt
        rpc_config Tezos_rpc_http.Media_type.all_media_types
  end

let call raw_url cctxt =
  let uri = Uri.of_string raw_url in
  let args = String.split_path (Uri.path uri) in
  RPC_description.describe cctxt ~recurse:false args
  >>=? function
  | Static {services; _} -> (
    match RPC_service.MethMap.find_opt `GET services with
    | None ->
        cctxt#message
          "No service found at this URL with this method (but this is a valid \
           prefix)\n\
           %!"
        >>= fun () -> return_unit
    | Some {input = Some _; _} ->
        cctxt#message "I'm not able to do post requests %!"
        >>= fun () -> return_unit
    | Some {input = None; _} ->
        cctxt#generic_json_call `GET uri >>= fun _ -> return_unit )
  | _ ->
      cctxt#message "No service found at this URL\n%!"
      >>= fun () -> return_unit
