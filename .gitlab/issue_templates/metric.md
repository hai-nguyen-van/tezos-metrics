
### What we want to monitor

( add here the new metric : ex: the total number of active connections )

### How are can extract this information

( add the how to compute this information: Ex: compute the length of /networks/connections at each tick )

### What is they type of this metric ?

- Gauge
- Histogram
- Summary

### How to display 

( what is the graph that you want to obtain : Ex display the number of connections over time )
