
all: build

build-deps:
	scripts/build-deps.sh

proj-deps:
	scripts/proj-deps.sh

build:
	eval $$(opam env) && dune build @main
	@cp _build/default/src/main.exe tezos-metrics

clean:
	dune clean

clean-dist:
	dune clean
	rm -Rf _opam

docker:
	docker build -t nomadiclabs/tezos-metrics docker
	docker push nomadiclabs/tezos-metrics

fmt:
	ocamlformat --inplace $(shell find src \( -name "*.ml" -o -name "*.mli"  -o -name "*.mlt" \) -type f -print)


.PHONY: docker
