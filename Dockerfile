FROM alpine:3.11 as builder

RUN apk --no-cache add \
  opam m4 build-base \
  gmp-dev libev-dev pcre-dev zlib-dev hidapi-dev \
  perl

WORKDIR /workdir

RUN opam init --bare --disable-sandboxing

RUN opam switch create 4.09.1
#opam remote add pirbo-tezos-2020-03 git+https://github.com/pirbo/opam-repository#tezos-2020-03
RUN opam install --yes dune.1.11.4

COPY Makefile /workdir/
COPY src/* /workdir/src/

###################################

FROM builder as project

RUN eval $(opam env) && \
    opam install --yes --deps-only src/

RUN make

###################################

FROM alpine:3.11 as final

RUN apk --no-cache add coreutils libev gmp
COPY --from=project /workdir/tezos-metrics /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/tezos-metrics"]
